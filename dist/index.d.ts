declare module 'jj-format' {
	/**
	 * 메소드 {@link Format.ofTime}의 값 설정을 위한 자료 구조
	 */
	export type FormatTimeConfig = {
		/**
		 * 초
		 */
		's'?: string;
		/**
		 * 분
		 */
		'm'?: string;
		/**
		 * 시/시간
		 */
		'h'?: string;
		/**
		 * 일
		 */
		'D'?: string;
		/**
		 * 월/개월
		 */
		'M'?: string;
		/**
		 * 년
		 */
	    'Y'?: string;
	};
	/**
	 * 주어진 자료 구조를 읽기 편하게 가공해 주는 유틸리티 클래스
	 */
	export default class Format {
	    /**
	     * 날짜 정보를 `YYMMDD_hhmmss` 형식으로 가공한 문자열을 반환한다.
	     *
	     * @param date 대상 날짜
	     */
	    static ofDate(date: Date): string;
	    /**
	     * 시간 정보를 단위별 접미사를 붙여 가공한 문자열을 반환한다.
	     *
	     * @param seconds 대상 시간(초)
	     * @param textConfig 단위별 접미사
	     * @example
	     * // 1시간 23분 45초
	     * console.log(Format.ofTime(5025, { 'h': "시간", 'm': "분", 's': "초" }));
	     */
	    static ofTime(seconds: number, textConfig?: FormatTimeConfig): string;
	    /**
	     * 수 정보에 천 단위 반점을 포함시킨 문자열을 반환한다.
	     *
	     * @param number 대상 수
	     * @example
	     * // 1,234,500
	     * console.log(Format.ofNumber(1234500));
	     */
	    static ofNumber(number: number): string;
	    /**
	     * 주어진 문자열의 가장 앞 글자만 대문자로 바꾸어 반환한다.
	     *
	     * @param text 대상 문자열
	     * @example
	     * // Apple
	     * console.log(Format.ofCapital("apple"));
	     */
	    static ofCapital(text: string): string;
	    /**
	     * 전체에 대한 부분의 비를 백분율로 나타낸 문자열을 반환한다.
	     *
	     * @param part 부분
	     * @param total 전체
	     * @param fixedDigit 소수점 아래 자릿수
	     * @example
	     * // 75.00%
	     * console.log(Format.ofRate(3, 4, 2));
	     */
	    static ofRate(part: number, total?: number, fixedDigit?: number): string;
	}

}
