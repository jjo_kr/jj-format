"use strict";
exports.__esModule = true;
/**
 * 주어진 자료 구조를 읽기 편하게 가공해 주는 유틸리티 클래스
 */
var Format = (function () {
    function Format() {
    }
    /**
     * 날짜 정보를 `YYMMDD_hhmmss` 형식으로 가공한 문자열을 반환한다.
     *
     * @param date 대상 날짜
     */
    Format.ofDate = function (date) {
        var _a = [
            date.getFullYear(), date.getMonth() + 1, date.getDate(),
            date.getHours(), date.getMinutes(), date.getSeconds()
        ], Y = _a[0], M = _a[1], D = _a[2], h = _a[3], m = _a[4], s = _a[5];
        function zero2(value) {
            return value < 10 ? "0" + value : value.toString();
        }
        return zero2(Y) + zero2(M) + zero2(D)
            + "_" + zero2(h) + zero2(m) + zero2(s);
    };
    /**
     * 시간 정보를 단위별 접미사를 붙여 가공한 문자열을 반환한다.
     *
     * @param seconds 대상 시간(초)
     * @param textConfig 단위별 접미사
     * @example
     * // 1시간 23분 45초
     * console.log(Format.ofTime(5025, { 'h': "시간", 'm': "분", 's': "초" }));
     */
    Format.ofTime = function (seconds, textConfig) {
        if (textConfig === void 0) { textConfig = {}; }
        var R = [];
        if (seconds >= 31536000)
            R.push(Math.floor(seconds / 31536000) + (textConfig.Y || "Y"));
        else if (seconds >= 2635200)
            R.push(Math.floor(seconds / 2635200) + (textConfig.M || "M"));
        else if (seconds >= 86400)
            R.push(Math.floor(seconds / 86400) + (textConfig.D || "D"));
        else {
            if (seconds >= 3600)
                R.push(Math.floor(seconds / 3600) + (textConfig.h || "h"));
            if (seconds >= 60)
                R.push(Math.floor(seconds / 60) % 60 + (textConfig.m || "m"));
            R.push(seconds % 60 + (textConfig.s || "s"));
        }
        return R.join(' ');
    };
    /**
     * 수 정보에 천 단위 반점을 포함시킨 문자열을 반환한다.
     *
     * @param number 대상 수
     * @example
     * // 1,234,500
     * console.log(Format.ofNumber(1234500));
     */
    Format.ofNumber = function (number) {
        var parts = number.toString().split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+\b)/g, ",");
        return parts.join('.');
    };
    /**
     * 주어진 문자열의 가장 앞 글자만 대문자로 바꾸어 반환한다.
     *
     * @param text 대상 문자열
     * @example
     * // Apple
     * console.log(Format.ofCapital("apple"));
     */
    Format.ofCapital = function (text) {
        return text[0].toUpperCase() + text.slice(1);
    };
    /**
     * 전체에 대한 부분의 비를 백분율로 나타낸 문자열을 반환한다.
     *
     * @param part 부분
     * @param total 전체
     * @param fixedDigit 소수점 아래 자릿수
     * @example
     * // 75.00%
     * console.log(Format.ofRate(3, 4, 2));
     */
    Format.ofRate = function (part, total, fixedDigit) {
        if (total === void 0) { total = 1; }
        if (fixedDigit === void 0) { fixedDigit = 2; }
        var rate;
        if (total === 0) {
            rate = 0;
        }
        else {
            rate = part / total * 100;
        }
        return rate.toFixed(fixedDigit) + "%";
    };
    return Format;
}());
exports["default"] = Format;
//# sourceMappingURL=index.js.map